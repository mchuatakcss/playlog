Quick start using "suggested" stack

- copy env-example to .env
- run `docker-compose up -d nginx mariadb redis`
- go to 127.0.0.1


Feel free to change .env config to whatever you want, use whatever containers 
you want, etc.  Just make sure to provide instructions on how to run when it 
gets sent back.  Further configuration instructions can be found at https://laradock.io
