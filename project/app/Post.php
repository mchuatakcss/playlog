<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable')->whereNull('parent_id');
    }

    // /**
    //  * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
    //  */
    // public function likes()
    // {
    //     return $this->belongsToMany(Post::class, 'likeunlike', 'likeunlike_id', 'post_id')->withTimestamps();
    // }

    // /**
    //  * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
    //  */
    // public function followings()
    // {
    //     return $this->belongsToMany(User::class, 'followers', 'follower_id', 'leader_id')->withTimestamps();
    // }    

}
