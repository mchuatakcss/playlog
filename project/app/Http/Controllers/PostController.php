<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Like;

class PostController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth');
    }

    public function index()
    {
        $posts = Post::all();
        $likes = Like::where('post_id', $request->get('post_id'))->get();
        $likes = $likes->count();
        
        return view('index', compact('posts', 'likes'));
    }

    public function create()
    {
        return view('post');
    }

    public function store(Request $request)
    {
        $post =  new Post;
        $post->body = $request->get('body');
        $post->user()->associate($request->user());

        $post->save();

        return redirect('newsfeed');

    }

    public function show($id = "")
    {
        if(!empty($id) ){
            $posts = Post::find($id);
        
            return view('show', compact('posts'));
        } else { 
            $posts = Post::all();

            // $likes = Like::where('post_id', $id)->get();
            // $likes = $likes->count();

            return view('show', compact('posts'));
        }
    }
  
}
