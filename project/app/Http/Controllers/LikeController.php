<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Like;

class LikeController extends Controller
{
    public function store(Request $request)
    {
        $likeByUser = Like::where('user_id', $request->user()['id'])
                          ->where('post_id', $request->get('post_id'))->value('id');
        
        if(!($likeByUser) ){
            $like =  new Like();
    
            $like->post()->associate($request->get('post_id'));
    
            $like->user()->associate($request->user());
    
            $like->save();
        }

        return redirect('newsfeed');

    }

    public function show($id = "")
    {
        $likeByUser = Like::where('user_id', $request->user()['id'])
        ->where('post_id', $request->get('post_id'))->value('id');

        if(!($likeByUser) ){
        $like =  new Like();

        $like->post()->associate($request->get('post_id'));

        $like->user()->associate($request->user());

        $like->save();
        }

        return redirect('newsfeed');
    }

}
