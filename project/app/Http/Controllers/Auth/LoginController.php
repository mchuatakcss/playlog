<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Hash;

use Faker\Generator as Faker;

use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Faker $faker)
    {
        $this->middleware('guest')->except('logout');
        $this->faker  = $faker;
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request)
    {

        //$this->validateLogin($request);
        $user = User::where('email', $request->email)->first(); // Something like User:: where() or whatever depending on your impl.
        
        if(!$user){ 

            $user = User::create([
                'name' => $this->faker->name,
                'email' => $request['email'],
                'password' => Hash::make($this->faker->text(8)),
            ]);         
            
        }

        Auth::login($user);
        
        return redirect()->intended('newsfeed');
    }

}
