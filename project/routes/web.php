<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Auth::routes();
// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/post/create', 'PostController@create')->name('post.create');
Route::post('/post/store', 'PostController@store')->name('post.store');

Route::get('/post/show/{id}', 'PostController@show')->name('post.show');
Route::get('/posts', 'PostController@index')->name('posts');

Route::post('/like/store', 'LikeController@store')->name('like.store');

Route::post('post/{id}/like', 'PostController@likePost')->name('post.like');
Route::post('/{id}/unlike', 'PostController@unlikePost')->name('post.unlike');

Route::get('/newsfeed', 'PostController@show')->name('newsfeed.all');

Route::post('/comment/store', 'CommentController@store')->name('comment.add');
Route::post('/reply/store', 'CommentController@replyStore')->name('reply.add');