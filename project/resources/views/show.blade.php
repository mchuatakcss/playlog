@extends('layouts.app')
@section('content')
<!-- <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css"> -->
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

<div class="container mb-2">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header-custom">Create Post</div>
                <div class="px-3">
                    <form method="post" action="{{ route('post.store') }}">
                        <div class="form-group">
                            @csrf
                            <label class="label">What's on your mind?</label>
                            <textarea name="body" rows="2" cols="30" class="form-control" required></textarea>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-success" value="Post Comment" />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@if(!empty($posts))
    @foreach($posts as $post)
    <div class="container mb-2">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-11">
                                <div class="media">
                                <div class="media-left">
                                    <a href="#" class="profile-pic">
                                    <img class="media-object photo-profile" src="/asset/dummy-icon.png" width="32" height="32" alt="...">
                                    </a>
                                </div>
                                <div class="media-body">
                                    <a href="#" class="anchor-username"><h4 class="media-heading">{{ $post->user->name }}</h4></a> 
                                </div>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <a href="#"><i class="glyphicon glyphicon-remove"></i></a>
                            </div>
                        </div>          
                        <p>{{ $post->body }}</p>
                        <hr />       
                        <div class="row">
                            <ul class="list-unstyled">
                                <form method="POST" action="{{ route('like.store', $post->id ) }}">
                                        <input type="hidden" name="post_id" value="{{ $post->id }}" />
                                        @csrf
                                    <button type="submit" class="btn">
                                        <li><i class="glyphicon glyphicon-thumbs-up"></i> Like </li>
                                    </button>
                                </form>
                                <!-- <form method="POST" action="{{ route('post.unlike', $post->id ) }}">
                                        <input type="hidden" name="post_id" value="{{ $post->id }}" />
                                        @csrf
                                    <button type="submit" class="btn">
                                        <li><i class="glyphicon glyphicon-thumbs-down"></i> Un-like</li>
                                    </button>
                                </form> -->
                            </ul>     
                        </div>      
                        <form method="post" action="{{ route('comment.add') }}" class="form-comment">
                            @csrf
                            <div class="form-group">
                                <input type="text" name="comment_body" class="form-control" />
                                <input type="hidden" name="post_id" value="{{ $post->id }}" />
                            </div>
                            <div class="form-group">
                                <input type="submit" class="btn btn-warning" value="Add Comment" />
                            </div>
                        </form>
                        @include('partials._comment_replies', ['comments' => $post->comments, 'post_id' => $post->id])
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endforeach

@else
Empty
@endif
@endsection
