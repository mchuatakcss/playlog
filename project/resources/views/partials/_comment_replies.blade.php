<style>
</style>
@foreach($comments as $comment)
    <div class="display-comment">
        <a href="#" class="profile-pic">
            <img class="media-object photo-profile" src="/asset/dummy-icon.png" width="32" height="32" alt="...">
        </a>    
        <div class="sub-comm">
            <strong>{{ $comment->user->name }}</strong> {{ $comment->body }}
        </div>
        <a href="" id="reply"></a>
        <form method="post" action="{{ route('reply.add') }}">
            @csrf
            <div class="form-group">
                <input type="text" name="comment_body" class="sub-comm-body" />
                <input type="hidden" name="post_id" value="{{ $post_id }}" />
                <input type="hidden" name="comment_id" value="{{ $comment->id }}" />

                <button type="submit" class="btn btn-default btn-sm">
                    <span class="glyphicon glyphicon-plus"></span>
                </button>                
            </div>
        </form>
        @include('partials._comment_replies', ['comments' => $comment->replies])
    </div>
@endforeach